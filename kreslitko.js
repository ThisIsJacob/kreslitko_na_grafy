var fs=require('fs');
var http = require('http');
const { exec } = require("child_process");
const cors=require('cors');

var pocitam_ted_neco=0, build_status=0, cu_key=0, em, kompletni_std, wait_compile=0;

http.createServer(function (req, res) {
    //console.log(req.headers);
    console.log(req.url);
    handle_request(req, res);
    //res.write('Hello World!'); //write a response to the client
    //res.end(); //end the response
}).listen(8080); //the server object listens on port 8080

function verify_compilation_end(response){
    wait_compile++;
    console.log("overuji jestli existuej graf.json");
    fs.readFile('tvoric_grafu/graf.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        if(data=="n" && wait_compile < 25){
            setTimeout(function(){
                verify_compilation_end(response);
            }, 200)
        }else{
            setTimeout(function(){
                console.log(kompletni_std)
                if(wait_compile < 25 && (kompletni_std=="" || em=="a")){
                    cu_key = makeid(6);
                    response.write(cu_key);
                    response.end();
                }else
                {
                    response.write(em);
                    response.end();
                }
            }, 100);
        }
    });
}

function handle_request(request, response){
    if(request.headers.a_min && request.headers.code && request.headers.a_max && request.headers.one_step){
        if(request.headers.one_step < 0.001) return "step_too_low";
        //console.log(request.headers.code);
        if(pocitam_ted_neco==0)
        {
            build_status=1;
            //var kompletni_std;
            pocitam_ted_neco=1;
            //vytvoreni souboru
            fs.writeFile('tvoric_grafu/graf.txt', request.headers.code, function (err) {
                if (err) throw err;
            });
            fs.writeFile('tvoric_grafu/obor_hodnot.txt', 'float a_min='+request.headers.a_min+', a_max='+request.headers.a_max+', step='+request.headers.one_step+';', function (err) {
                if (err) throw err;
            });
            //Provedeni výpočtu
            em="a";
            exec("cd tvoric_grafu && sleep 0.05 && ./vytvor_soubor.sh && sleep 0.05 && gcc main.c graf.c graf.h -lm -o l && screen -dmS L ./l", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    em = error.message;
                    //return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log(`stdout: ${stdout}`);
                kompletni_std = stdout;
            });
            fs.writeFile('tvoric_grafu/graf.json', "n", function (err) {
                if (err) throw err;
            });
            wait_compile=0;
            verify_compilation_end(response);
            setTimeout(function () {
                if(build_status==1){
                    cu_key=0;
                    pocitam_ted_neco=0;
                }
            }, 6000)

        }
        else
        {
            response.write('wait');
            response.end();
        }
        
        return;
    }
    else if(request.url=="/api" && request.headers.key == cu_key){
        build_status = 0;
        cu_key=0;
        pocitam_ted_neco=0;
        fs.readFile('tvoric_grafu/graf.json', 'utf8', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        });
    }else if(request.url=="/utils.js"){
        fs.readFile('web/utils.js', 'utf8', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        }); 
    }else if(request.url=="/Chart.min.js"){
        fs.readFile('web/Chart.min.js', 'utf8', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        }); 
    }else if(request.url=="/nacitani.gif"){
        fs.readFile('web/nacitani.gif', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.writeHead(200, {'Content-Type': 'image/gif'});
            response.write(data);
            response.end();
        }); 
    }else if(request.url=="/gitlab.png"){
        fs.readFile('web/gitlab.png', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.writeHead(200, {'Content-Type': 'image/png'});
            response.write(data);
            response.end();
        }); 
    }else{
        fs.readFile('web/index.html', 'utf8', function (err,data) {
            if (err) {
              return console.log(err);
            }
            console.log("RU:"+request.url);
            response.write(data);
            response.end();
        }); 
    }
}


function makeid(length) { // toto jsem zebral z https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
